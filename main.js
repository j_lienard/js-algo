"use strict";
exports.__esModule = true;
var user_collection_1 = require("./src/collection/user-collection");
var user_1 = require("./src/models/user");
var Main = /** @class */ (function () {
    function Main() {
        var userCollection = new user_collection_1.UserCollection();
        var user = new user_1.User();
        user.lastName = 'Lienard';
        user.firstName = 'Jeff';
        user.setId('jlienard');
        user.setPassword('admin');
        console.log(user.toString());
        userCollection.add(user);
        userCollection.add(user);
        console.log("Collection contains : ".concat(userCollection.size(), " item"));
        /*
        const id: string = 'toro';
        const password: string = 'tiri';
        
        const login: Login = new Login();
        login, setCredentials(id, password);
        const result = login.process();
        if (result) {
          console.log('Yes you can');
        } else {
    
        }
        */
    }
    return Main;
}());
// Load Main
new Main();
