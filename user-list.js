class User {
    constructor() {
        this.name = '';
        this.firstName = '';
        this.id = '';
        this.password = '';
    }
}
/** 
unique(user : User, list: User[])
@param user: User Une variable de type User
@param list: User[] un tableau de User
@returns boolean
*/ 

function unique(user, list) {
    let YesYouCan = true;
    for (let i=0; i < list.length; i++) {
        if (list[i].id === user.id) {
            YesYouCan = false;
        }
    }
    return YesYouCan;
}

/**
 * 
 * @param {*} user Some user variable
 * @param {*} list A list of users
 * @return User[]
 */

function ajouter(user, list) {
    if (unique(user, list)) {
        list.push(user);
    }
    return list;
}

let listUsers = [];

const user = new User();
user.name = 'Lienard';
user.firstName = 'Jeff';
user.id = 'jlienard';
user.password = 'admin';

const user2 = new User();
user2.name = 'Lie';
user2.firstName = 'Jak';
user2.id = 'jakl';
user2.password = 'admin2';

listUsers = ajouter(user, listUsers);
listUsers = ajouter(user2, listUsers);

console.log(listUsers.length);

listUsers.forEach((user) => {
    console.log( user.name + ' ' + user.id )
})






