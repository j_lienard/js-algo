/**
 * main
 * @author Moi <moi@moi.com>
 * Simple approche algorithmique :
 *  - définition de variables
 *  - affectations de valeurs aux variables
 *  - Les différents types scalaires
 * @version 1.0.0
 */

let myName = '';
let yourName = '';

// Affectation de valeur Aubert à la variable myName
myName = 'Aubert';
yourName = 'Bond';

// Affichage du contenu de la variable myName
console.log(myName); // Expected output : Aubert

// Changer la valeur de myName par autre chose
myName = 'Autre chose';
console.log(myName); // Expected output : Autre chose

let isMale = true;
let hasADog = true;

let users = [
    'Aubert', // 0
    'Abdel', // 1
    'Nesibe', // 2
    'Rainui', // 3
    'Rémy' // 4
];

// Affecter le contenu d'une variable à une autre variable
myName = yourName;
yourName = 'Superman';
console.log(`${myName} <=> ${yourName}`);

for (let index = 0; index <= 4; index = index + 1) {
    console.log(users[index]);
}

let index = 0;
while (index < users.length) {
    console.log(users[index]);
    index = index + 1;
}
// index vaut 5
do {
    console.log(users[index]);
    index = index + 1;
} while (index < users.length)

if (users[0] == 'Aubert') {
    console.log(`Salut ${users[0]}`);
}

if (users[3] == 'Aubert') {
    console.log(`Hola ${users[3]}`);
} else {
    console.log(`T'es pas Aubert, du coup salut ${users[3]}`);
}

let isACat = null;

if (isACat) { // Est ce que 0 est vrai
    console.log('Je suis vrai');
} else {
    console.log('Je suis faux');
}

// Ternaire
(users[0] === 'Aubert') ? 
    console.log('Okay JL') :
    console.log('Pas JL');
let isJL = users[0] === 'Aubert';

let anyValue = 1;
switch (anyValue) {
    case false:
        console.log('Hey m true');
        break;
    case 0:
    case 1:
    case 2:
        console.log('Je fais partie de 0..2');
        break;

    case 3:
        console.log('Je suis seulement 3');
        break;

    default: 
        console.log('Je ne suis rien !');
}
