# Algorithmique

## Variables

Une variable est un espace mémoire qui sera réservé pour une donnée typée.

Les types "scalaires" : chaîne, nombre, booléens

Les types "tableaux" : contenant une série de données, typées

Les types "complexes" : contenant différents types de données

Une variable peut voir son contenu être changé en cours d'exécution de
l'algorithme

### Exemples en algorithmique

`myName: Chaine`

`myArray: Chaine[]`

`index: Nombre`

`isOld: Booleen`

En algorithmique, toute variable doit être définie avant son utilisation.

***Note :***
Le nommage des variables devra respecter une convention dite : "camelCase".

Les types sont définis en "PascalCase" (majusucule au début et sur les mots qui composent le nom de la variable).

### Affectations

Une fois qu'une variable a été définie, on peut lui **affecter** une valeur.

`myName <- "Aubert"`

Dans cet exemple, l'espace mémoire *myName* va stocker la valeur **Aubert** jusqu'à ce qu'on change son affectation.

**ATTENTION** en algorithmique, l'opérateur d'affectation est le signe `<-` et pas `=` pour éviter de confondre avec l'opérateur de *comparaison*

On utilisera en algorithmique le caractère " (double-quote) pour contenir une chaîne de caractères.

On peut affecter à une variable le contenu d'une autre variable ou le résultat de l'exécution d'une fonction :

`myName: Chaine`

`otherName: Chaine`

`myName <- "Aubert"`

`otherName <- myName`

`myName <- lire()`

`ecrire(otherName)`

`ecrire(myName)`

Dans ce cas, la "sortie" serait quelque chose du genre :

`Aubert`

`something else`

***Note :***

Les pseudo fonctions `lire()` et `ecrire()` permettent respectivement de lire une données depuis une source externe, écrire envoie une information vers une destination quelconque (écran, imprimante, fichier, ...)

## Les boucles

Il existe deux types de boucles :
 - Les boucles inconditionnelles (POUR),
 - Les boucles conditionnelles (TANT QUE, REPETER)

`users: Chaine[] = ["Aubert", "Bond", "Superman"]`

### Avec une boucle POUR
```
POUR index: Entier DE 0 à 2 INCREMENT 1
    ecrire(users[index])
FIN POUR
```

### Boucle TANT QUE

```
index: Entier <- 0
TANT QUE index < long(users) FAIRE
    ecrire(users[index])
    index <- index + 1
FIN TANT QUE
```
Dans le cas d'un TANT QUE les instructions peuvent ne jamais être exécutées, la condition étant évaluée avant de rentrer dans le corps de la boucle.

### Boucle REPETER...JUSQU'A

```
index: Entier <- 0
REPETE
    ecrire(users[index])
    index <- index + 1
JUSQU'A index > 4
```
Dans le cas d'un REPETE l'instruction sera exécutée au moins une fois, la condition de sortie étant évaluée à la fin de la boucle.

## Traitement conditionnel

Evaluer une expression booléenne et décider de ce qu'on fait si la condition est vraie, et éventuellement ce qu'on fait si la condition est fausse.

```
SI expression_logique ALORS
    ...
    ...
FIN SI

SI expression_logique EST VRAI ALORS
    ...
    ...
SINON
    ...
    ...
FIN SI

### Expressions logiques

- = (ATTENTION == dans les langages)
- <, >, >=, <=
- <> (ATTENTION != dans les langages),
- ET, OU, NON (AND, OR, NOT) (dans les langages, vous trouverez &&, ||, !)

### Expressions ternaires

Parfois on a besoin d'évaluer "en ligne" une condition. L'opérateur "ternaire" va offrir une solution pour ce type d'opérations :

`expression_logique ? instruction_si_vrai : instruction_si_faux`

### SELECTION CAS
anyValue <- 3
SELECTION anyValue
CAS 0 FAIRE
    ...
CAS 1 FAIRE
    ...
CAS 3
    ...
DEFAUT
    ...
FIN SELECTION






