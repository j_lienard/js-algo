
// Chaque charac doit être stocké en tant que nombre dans un PC
// Méthode de chiffrement par substitution, chaque lettre est remplacée par décalage vers le haut suivant le param nb
function encrypt(charac, nb) {
    // stocker le résultat
    let word = '';
    // parcours chaque charactère
    for (let i=0; i < charac.length; i++) {
        // récup du charactère
        let c = charac[i];
        // récup du code du charactère en hexa
        let code = charac.charCodeAt(i);
        // récup du charactère + offset
        c = String.fromCharCode((code) + nb);
    // remplacement du mot rentré par le mot résultant du décalage par nb
    word +=c;
    }
    // résultat
    return word;
};
console.log(encrypt("Admin",5));

function decrypt(charac, nb) {
    // stocker le résultat
    let word = '';
    // parcours chaque charactère
    for (let i=0; i < charac.length; i++) {
        // récup du charactère
        let c = charac[i];
        // récup du code du charactère en hexa
        let code = charac.charCodeAt(i);
        // récup du charactère + offset
        c = String.fromCharCode((code) - nb);
    // remplacement du mot rentré par le mot résultant du décalage par nb
    word +=c;
    }
    // résultat
    return word;
};
console.log(decrypt("Firns", 5));

