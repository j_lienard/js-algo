let userAges = [
    [
        'Jean-Luc', 53
    ],
    [
        'Bond', 35
    ],
    [
        'Joker', 75
    ]
];

// 1. Afficher la liste des utilisateurs avec leur âge
//  Jean-Luc 53
//  Bond 35
//  Joker 75
let expectedResult = 'Jean-Luc 53 \nBond 35 \nJoker 75 \n';
let realResult = '';
for (let bigTableIndice = 0; bigTableIndice < userAges.length; bigTableIndice = bigTableIndice + 1) {
    let user = userAges[bigTableIndice];
    for (let smallTableIndice = 0; smallTableIndice < user.length; smallTableIndice = smallTableIndice + 1) {
        realResult = realResult + user[smallTableIndice] + ' ';
    }
    realResult = realResult + '\n';
}
if (expectedResult === realResult) {
    console.log(realResult);
} else {
    console.log('Try again rookie');
}
// 2. Dites bonjour aux utilisateurs de moins de 50 ans
//  Bonjour Bond
userAges
    .filter((userDetail) => userDetail[1] < 50)
    .map((userDetail) => userDetail[0])
    .forEach((user) => {
        console.log('Bonjour ' + user);
    });

// 3. Calculer l'âge moyen des utilisateurs
//  Attendu 54.3333333333
console.log(
    userAges.map((userDetail) => userDetail[1])
    .reduce((prevAge, nextAge) => prevAge + nextAge) / userAges.length
);